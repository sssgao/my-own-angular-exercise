import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Post} from '../../post';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() markPost: Post;

  @Output() update: EventEmitter<Post>;

  postForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.update = new EventEmitter();
  }

  ngOnInit() {
  this.postForm = this.fb.group({
    id: new FormControl('',[Validators.required]),
    userId: new FormControl('', [Validators.required]),
    title: new FormControl('', [Validators.required, Validators.minLength(5)]),
    body: new FormControl('')
  });

    this.postForm.patchValue(this.markPost);

  }

  mySubmit(){
    console.log("hello world");
    console.log ("i am touched");
    this.update.emit(this.postForm.value);

  }


}
