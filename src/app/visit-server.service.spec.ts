import { TestBed } from '@angular/core/testing';

import { VisitServerService } from './visit-server.service';

describe('VisitServerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VisitServerService = TestBed.get(VisitServerService);
    expect(service).toBeTruthy();
  });
});
