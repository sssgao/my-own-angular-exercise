import { Component, OnInit } from '@angular/core';
import {Post} from '../../post';
import {VisitServerService} from '../../visit-server.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {



  constructor(private postService: VisitServerService) {
    this.posts = [];
    this.needShowLoader = false;


  }

  posts: Post[];
  needShowLoader: boolean;

  ngOnInit() {
    this.getPosts();
  }





  getPosts() {
    this.needShowLoader = true;

    setTimeout(() => {
      this.postService.getOrders().subscribe((res: Post[]) => {
        this.posts = res;
        this.needShowLoader = false;
      });
    }, 1000);


  }

  createPost() {
    this.postService.createOrder();

  }

  deletePost() {
    let post = this.posts.filter(ele => ele.id === 7);
    this.postService.deleteOrder(post[0]);

  }




  // updatePost(post: Post){
  //
  //   this.postService.updateOrder(post);
  //
  // }

  onUpdatePost(evt: Post){

    this.postService.updateOrder(evt);

  }
}
