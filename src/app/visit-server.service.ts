import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Post} from './post';

@Injectable({
  providedIn: 'root'
})
export class VisitServerService {

  posts: any;
  serverUrl: string;


  constructor(private httpService: HttpClient) {
    this.serverUrl = 'http://localhost:3000/posts';
    this.posts = undefined;
  }


  getOrders() {


    return this.httpService.get(this.serverUrl);

  }

  createOrder() {

    let post: Post = {userId: 888, id: 333, body: 'testtest', title: 'testTitle'};
    this.httpService.post(this.serverUrl, post).subscribe(res => console.log(res));

  }

  deleteOrder(post:Post) {
    let tmp = this.serverUrl + '/' + post.id;
  this.httpService.delete(tmp).subscribe(res => console.log(res));
  }

  updateOrder(pp:Post){
    this.httpService.put(this.serverUrl+'/6',pp).subscribe();

  }
}
